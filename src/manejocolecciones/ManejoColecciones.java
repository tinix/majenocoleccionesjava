
package manejocolecciones;
import java.util.*;


/**
 *
 * @author tinix
 */
public class ManejoColecciones {

    public static void main(String[] args) {
        List miLista = new ArrayList();
        miLista.add("1");
        miLista.add("2");
        miLista.add("3");
        miLista.add("4");
        // Element repetido
        miLista.add("4");
        imprimir(miLista);
        
        Set miSet = new HashSet();
        miSet.add("100");
        miSet.add("200");
        miSet.add("300");
        // No permite elementos repetidos lo ignora
        miSet.add("300");
        imprimir(miSet);
        
        
        Map miMapa = new HashMap();
        miMapa.put("1", "Juan");
        miMapa.put("2", "Tinix");
        miMapa.put("3", "George");
        miMapa.put("4","Daniel");
        // Se imprime todas las llaves
        imprimir(miMapa.keySet());
        imprimir(miMapa.values());
       
    }
    
    public static void imprimir(Collection coleccion){
        for (Object elemento : coleccion) {
            System.out.println(elemento + " ");
        }
        System.out.println("");
    }
    
}
